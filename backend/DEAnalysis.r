# pull in libraries
library(magrittr)
library(edgeR)
library(data.table)
source('DETools.r')

###---------------------------------------------------------------------------------------
# Load and Clean data
###---------------------------------------------------------------------------------------
x <- 
    read.delim(file = "countMatrix.tsv", row.names = 1) %>% 
    # removing all genes that do not express at all anywhere
    .[rowSums(.) != 0,] %>%                   
    # this is a limma function that. removing annotation version from ENEMBL IDs
    set_rownames(., strsplit2(rownames(.), split = "\\.")[,1]) 

bead(x) # big head from DETools.r

# pull in the annotation
anno <- readRDS("anno.rds") %>% .[!duplicated(.$gene_name),]
look(anno) # look at head and tail from DETools.r

# THESE MUST PASS OR THINGS WILL BREAK
# MUST BE gene_id (sorry)
sum(rownames(x) %in% anno$gene_id) > 0.5*nrow(x) # at least 50% of genes annotated

# matching annotation and data table 
x <- x[rownames(x) %in% anno$gene_id,]
anno <- anno[match(rownames(x), anno$gene_id),]
identical(rownames(x), anno$gene_id)

# Make a table of variables (phenodat, targets, whatever you like to call them)
# Start with the names of x (we'll fix them later if they are ugly now)
### NOT RUN (run this if you want to make your own experimental variable table)
# colnames(x) %>% data.frame() %>% write.csv(x = , file = "exampleVars.csv", row.names = F)
### END NOT RUN

# After editing the csv file above, load it back into R
# An alternative to this is just build the data.frame in R
experimentVars <- 
    fread(file = "exampleVars.csv", stringsAsFactors = TRUE, header = T, data.table = F) %>% 
    set_rownames(., .[,1]) %>% .[-1]

look(experimentVars)

identical(colnames(x), rownames(experimentVars))

###---------------------------------------------------------------------------------------
# Check for Sex
###---------------------------------------------------------------------------------------

# A lot of times we don't know the sex so we find it from XIST and DDX3Y expression
### for Humans: XIST - ENSG00000229807 ; DDX3Y - ENSG00000067048
### for Mice: Xist - ENSMUSG00000086503 ; Ddx3y - ENSMUSG00000069045
### for Rats: Xist -  ; Ddx3y - ENSRNOG00000002501

# isFemale <-
#     x[grep("ENSMUSG00000086503|ENSMUSG00000069045", rownames(x)),] %>% t() %>%
#     apply(., 1, function(x) { x[1] > x[2] })

# optionally assign this to a variable
# sex <- sapply(isFemale, ifelse, "Female", "Male") %>% factor()

# alternatively just check counts on the Y chromosome
# colSums(x[anno$seqnames == "chrY",]) %>% data.frame(Ycounts = .)


###---------------------------------------------------------------------------------------
# Create Experiment Information Object
###---------------------------------------------------------------------------------------

# this function also generates some "pretty" names 
# excludeVarInNames -- exlude any variables you want from the new names
experiment <- buildExperiment(countMatrix = x, 
                              experimentName = "Rat.Tendon.PGDS", # use a meaningful name
                              variableTable = experimentVars, # table of variables
                              excludeVarInNames = "group") # same as c("treatment", "time")

# confirm new names are correct
data.frame(experiment$newNames)
# rename your original data (old names are stored in the experiment object)
colnames(x) <- experiment$newNames

### --------------------------------------------------------------------------------------
# Perform Differential Expression Analysis
### --------------------------------------------------------------------------------------

# create a factor for group info
group <- 
    experiment$experimentVariables$group %>% 
    factor(., levels = c("CTRL_Day0", "DrugY_Day7", "DrugY_Day21", 
                         "DrugG_Day7", "DrugG_Day21",  "DrugT_Day21", "DrugT_Day7"))

# create factor for donor 
donor <- experiment$experimentVariables$donor

# design no intercept model
design <- 
    model.matrix(~ 0 + group + donor) %>% 
    set_colnames(., gsub("group|donor", "", colnames(.))) %>%
    set_rownames(., colnames(x))

# Contrast Matrices must be generated manually
c_matrix <- makeContrasts(
    # Standard A-B Contrasts
    DrugY_Day7 - CTRL_Day0,   
    DrugG_Day7 - CTRL_Day0,
    DrugT_Day7 - CTRL_Day0,
    # ANOVA-like Contrasts
    (DrugG_Day21 + DrugG_Day7)/2 - (DrugY_Day21 + DrugY_Day7)/2,
    # Interaction Term
    (DrugG_Day21 - DrugG_Day7) - (DrugY_Day21 - DrugY_Day7),
    levels = design)

# using the date is convenient when you need to reanalyze later
file.prefix <- paste("edgeR_DEGs", Sys.Date(), sep = "_")
# gmt file from msigdb
gmt.path <- "c2.cp.v6.2.symbols.gmt"

KOvWT <- getDE(# first the data 
               counts          = x,          # a count matrix
               anno            = anno,       # the annotation data.frame
               experiment      = experiment, # the buildExperiment output 
               design          = design,     # the model.matrix output
               contrast        = c_matrix,   # the makeContrasts output
               group           = group,      # the grouping variable
               nuisanceVars    = "donor",    # a character string or character vector of variables to remove
               bioReps         = 5,          # if set to 1, then no dispersion calcuated
               # filtering parameters
               cutoff          = 3,          # minimum CPMs per group
               minGroupCutoff  = 1,          # minimum number of groups  
               minPathwayGenes = 10,         # minimum number of genes in a pathway
               maxPathwayGenes = 5000,       # maximum number of genes in a pathway
               # modeling parameters
               prior           = 0.2,         # 
               minLogFC        = 0,           # setting this non-0 will use glmTreat
               useFitMethod    = "qlf",       # glmFit or glmQLFit 
               useStatMethod   = "qlf",       # see glmLRT or glmQLFTest
               # output parameters
               out.prefix      = file.prefix, # saved file name
               countNormMethod = "cpm",       # cpms, tpms, rpkms...
               log.t           = TRUE,        # log transform CPMs? 
               save.xlsx       = TRUE,        # save an xlsx of the analysis
               doQuSage        = TRUE,        # run pathway or GO analysis with QuSAGE?
               path.gmt        = gmt.path     # gmt file for pathway or GO analysis
)

saveRDS(object = KOvWT, file = paste0(file.prefix, ".rds"))

### NOT RUN 
# To check the results interactively you need to move the rds to the "frontend" directory
# (any directory containing, app.R, global.r, www, and your resutls as .rds will work)
# system('cp edgeR_DEGs_*.rds ../frontend')
# system('cp 'RegNetwork_hu_mm_combined_no_mirna.csv ../frontend')
# shiny::runApp("../frontend")
### END NOT RUN




